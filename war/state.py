#
# This file is part of warlords.
# 
# warlords is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# warlords is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with warlords.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python
''' The Game State
Within lies the state of a game instance.
'''

import war
import war.grid
from war import LOG

class Game:
    ''' Game
    Keeps the state of the current game
    '''

    def __init__(self, size=(30,30)):
        self.grid = war.grid.Grid(size=size)

        
