#
# This file is part of warlords.
# 
# warlords is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# warlords is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with warlords.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python
from collections import defaultdict
import random

class User:
    ''' top level user '''

    def __init__(self, addr, nick='newb'):
        self.addr = addr
        self.nick = nick

class Chan:
    ''' room to hold logs and user lists '''

    def __init__(self):
        self.users = set()
        self.log = []
        self.user_seen = defaultdict(int)
    
    def join(self, user):
        self.users.add(user)
        self.user_seen[user] = len(self.log)
        self.log.append(('SERVER', '%s joined.' % user.nick))

    def part(self, user):
        if user in self.users:
            self.users.remove(user)
            self.log.append(('SERVER', '%s left.' % user.nick))

    def say(self, user, msg):
        if user not in self.users:
            return
        self.log.append((user.nick, msg))

    def get_log(self, user):
        if user not in self.users:
            return []
        entries = self.log[self.user_seen[user]:]
        self.user_seen[user] = len(self.log)
        return entries

class ChatRoom:
    ''' relay chat messages '''
    
    def __init__(self):
        self.userlist = {}
        self.chan = defaultdict(Chan)
        self.server = User('LOCAL')
        self.server.nick = 'SERVER'
        self.nicks = set(['SERVER'])
        self.user_lastjoin = {}

    def add_user(self, addr):
        #if addr in self.userlist:
        #    raise RuntimeError('already initiated a connection from %s' % addr)
        nick = None
        while not nick:
            nick = 'newb_%d' % random.randint(1000,9999)
            if nick in self.nicks:
                nick = None
        self.nicks.add(nick)
        user = User(addr, nick=nick)
        self.userlist[addr] = user
        self.join(addr, 'main')

    def disc_user(self, addr):
        if addr in self.userlist:
            user = self.userlist[addr]
            for channel in self.chan:
                self.chan[channel].part(user)
            self.nicks.remove(user.nick)
            del self.userlist[addr]
        else:
            raise RuntimeError('cant delete nonexistent user from %s' % addr)

    def join(self, addr, chan):
        user = self.userlist[addr]
        self.user_lastjoin[user] = chan
        self.chan[chan].join(user)

    def part(self, addr, chan):
        user = self.userlist[addr]
        self.chan[chan].part(user)

    def renick(self, addr, name):
        if addr not in self.userlist:
            raise RuntimeError('user cannot renick, no user from %s' % addr)
        user = self.userlist[addr]
        if name in self.nicks:
            last = self.user_lastjoin[user]
            self.say(addr, last, 'invalid nick')
            raise RuntimeError('user cannot renick, nick exists (%s)' % addr)
        self.nicks.remove(user.nick)
        old_name = user.nick
        user.nick = name
        self.nicks.add(user.nick)
        self.broadcast('%s changed nick to %s' % (old_name, name))

    def broadcast(self, msg):
        for channel in self.chan:
            chan = self.chan[channel]
            chan.say(self.server, msg)

    def say(self, addr, chan, msg):
        user = self.userlist[addr]
        self.chan[chan].say(user, msg)

    def get_log(self, addr, chan):
        user = self.userlist[addr]
        return self.chan[chan].get_log(user)

