#
# This file is part of warlords.
# 
# warlords is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# warlords is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with warlords.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python

from Tkinter import *

from war.net_pb2 import Request, Response
from war.net import ClientRequest, send_request, get_response

from war import LOG

class ChatWindow(object):

    def __init__(self):
        self.window = Tk()
        self.window.minsize(300, 500)
        self.window.maxsize(300, 500)
        self.chat = Listbox(self.window)
        self.chat.pack(fill=BOTH, expand=1)
        self.scroll = Scrollbar(self.chat)
        self.scroll.pack(side=RIGHT, fill=Y)
        self.chat.configure(yscrollcommand=self.scroll.set)
        self.scroll.configure(command=self.chat.yview)
        self.entry = Entry(self.window)
        self.entry.bind('<Return>', self.send_chat)
        self.entry.pack(fill=X, anchor=S)
        self.entry.focus_set()
        self.poll_chat()
        mainloop()

    def send_chat(self, event):
        msg = event.widget.get()
        if msg.startswith('/nick'):
            nick = msg.split()[1]
            req = ClientRequest(Request.NICK, nick=nick)
        elif msg.startswith('/join'):
            room = msg.split()[1]
            req = ClientRequest(Request.JOIN, room=room)
        else:
            req = ClientRequest(Request.CHAT, room='main', msg=msg)
        send_request(req)
        event.widget.delete(0, END)

    def poll_chat(self):
        req = ClientRequest(Request.LOG, room='main')
        send_request(req)
        res = get_response(req)
        while not res:
            res = get_response(req)
        for entry in res.log.entries:
            txt = '%13s: %s' % (entry.nick, entry.msg)
            self.chat.insert(END, txt)
        self.chat.yview(END)
        self.window.after(500, self.poll_chat)

