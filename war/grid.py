#
# This file is part of warlords.
# 
# warlords is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# warlords is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with warlords.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python
''' grid
used by the game state to keep track of our hex grid
'''

from collections import defaultdict, namedtuple

from war import LOG

class TILE: 
    (
        grass,
        manasand,
    ) = range(2)


class Tile(object):
    ''' a tile factory and superclass '''
    __slots__ = ['x', 'y', 'typ']

    def __init__(self, x, y):
        ''' just set up positions '''
        self.x, self.y = x, y
        self.typ = None

    @staticmethod
    def get(self, typ, x, y):
        ''' produce tile '''
        tile = TILE_CLASSES[typ](x, y)
        return tile

class GrassTile(Tile):
    ''' a grassy tile '''

    def __init__(self, x, y):
        Tile.__init__(self, x, y)
        self.typ = TILE.grass

class ManasandTile(Tile):
    ''' a manasand tile '''

    def __init__(self, x, y):
        Tile.__init__(self, x, y)
        self.typ = TILE.manasand
    
TILE_CLASSES = {
    TILE.grass: GrassTile, 
    TILE.manasand: ManasandTile,
}

class Grid(object):
    ''' the hex grid '''

    def __init__(self, width, height):
        ''' set up width and height (as squares) '''
        self.width = width
        self.height = height
        self.tiles = defaultdict()
        for x in xrange(width):
            for y in xrange(height):
                if (x + y) % 2 == 1:
                    self.tiles[(x, y)] = TILE_CLASSES[TILE.manasand](x, y)

    def get_tile(self, x, y):
        return self.tiles[(x, y)]

if __name__ == '__main__':
    import sys
    if '-d' in sys.argv:
        LOG.setLevel('DEBUG')
    LOG.debug('Starting grid test')
    g = Grid(100, 200)
    LOG.debug('Finished grid test')

