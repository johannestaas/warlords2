#
# This file is part of warlords.
# 
# warlords is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# warlords is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with warlords.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python

import threading
import SocketServer
import struct

import war.chat
import war.world
from war.net import get_request
from war.net_pb2 import Request, Response
from war import LOG

CHAT = None
WORLD = None

def print_all_chatlogs():
    for channel in get_chatroom().chan:
        chan = get_chatroom().chan[channel]
        print('#%s' % str(channel))
        for nick, msg in chan.log:
            print('%-10s: %s' % (nick, msg))

def get_chatroom():
    global CHAT
    if not CHAT:
        CHAT = war.chat.ChatRoom()
    return CHAT

def get_world():
    global WORLD
    if not WORLD:
        WORLD = war.world.World()
    return WORLD

def handle(addr, msg):
    req = get_request(msg)
    typ = req.type
    room = get_chatroom()
    if typ == req.CHAT:
        room.say(addr, req.chat.room, req.chat.msg)
    elif typ == req.JOIN:
        room.join(addr, req.join.room)
    elif typ == req.PART:
        room.part(addr, req.part.room)
    elif typ == req.NICK:
        try:
            room.renick(addr, req.nick.nick)
        except RuntimeError:
            pass
    elif typ == req.LOG:
        res = Response()
        res.tag = req.tag
        res.type = res.LOG
        res.log.room = req.log.room
        for nick, msg2 in room.get_log(addr, req.log.room):
            entry = res.log.Entry()
            entry.nick = nick
            entry.msg = msg2
            res.log.entries.extend([entry])
        ret = res.SerializeToString()
        return ret
    else:
        LOG.error('cant handle %s from %s' % (msg, addr))
        return 'errored'
    return


class WarHandler(SocketServer.StreamRequestHandler):
    ''' handle conex '''

    def handle(self):
        addr = self.client_address[0]
        LOG.info('user joined from %s' % addr)
        get_chatroom().add_user(addr)
        while True:
            msg = self.get_data()
            if msg is None: break
            out = handle(addr, msg)
            if out:
                self.send_data(out)
        
    def send_data(self, data):
        header = struct.pack('I', len(data))
        self.wfile.write(header)
        self.wfile.write(data)

    def get_data(self):
        length = self.rfile.read(4)
        if length == '':
            get_chatroom().disc_user(self.client_address[0])
            return None
        header = struct.unpack('I', length)
        length = header[0]
        return self.rfile.read(length)


class WarServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    ''' Serve some gamez 
    Call shutdown to kill
    '''

    def start(self):
        thread = threading.Thread(target=self.serve_forever)
        thread.daemon = True
        thread.start()
        LOG.debug("server loop running in thread: %s" % thread.name)

    def kill(self):
        LOG.info('shutting down server')
        self.shutdown()


