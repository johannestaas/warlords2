#
# This file is part of warlords.
# 
# warlords is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# warlords is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with warlords.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python
''' net-twerking
twerk that packet baby
twerk that shit all night

anotherwords client
'''

import socket
import struct

from war import LOG
from war import net_pb2

CONN = None
RESPONSES = {
    net_pb2.Response.LOG: {},
}

def poll_server():
    ''' run this in a separate thread '''
    global RESPONSES
    msg = read_data()
    if msg:
        res = net_pb2.Response()
        res.ParseFromString(msg)
        RESPONSES[res.type][res.tag] = res
    else:
        return
    poll_server()

def get_response(client_request):
    global RESPONSES
    request = client_request.request
    tag = request.tag
    typ = request.type
    if tag not in RESPONSES[typ]:
        return None
    return RESPONSES[typ][tag]

def connect(host, port):
    global CONN
    print('Connecting to %s:%s' % (host, str(port)))
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, port))
    CONN = sock

def disconnect():
    global CONN
    print('shutting down socket')
    CONN.shutdown(socket.SHUT_RDWR)
    print('closing socket')
    CONN.close()
    print('closed socket')
    CONN = None

def read_data():
    header = CONN.recv(4)
    if not header:
        return None
    length = struct.unpack('I', header)[0]
    ret = CONN.recv(length)
    if not ret:
        return None
    return ret

def send_data(msg):
    header = struct.pack('I', len(msg))
    CONN.sendall(header)
    CONN.sendall(msg)

def send_request(req):
    send_data(str(req))

def get_request(msg):
    req = net_pb2.Request()
    req.ParseFromString(msg)
    return req

class ClientRequest(object):
    __slots__ = ['request']
    tag_ct = 0
    
    def __init__(self, typ, **kwargs):
        req = net_pb2.Request()
        req.type = typ
        if typ == req.CHAT:
            req.chat.room = kwargs['room']
            req.chat.msg = kwargs['msg']
            if 'dest' in kwargs:
                req.chat.dest = kwargs['dest']
        elif typ == req.JOIN:
            req.join.room = kwargs['room']
        elif typ == req.PART:
            req.part.room = kwargs['room']
            if 'reason' in kwargs:
                req.part.reason = kwargs['reason']
        elif typ == req.NICK:
            req.nick.nick = kwargs['nick']
        elif typ == req.LOG:
            req.log.room = kwargs['room']
        else:
            raise RuntimeError('Invalid params to ClientRequest init')
        ClientRequest.tag_ct += 1
        req.tag = ClientRequest.tag_ct
        self.request = req

    def __str__(self):
        return self.request.SerializeToString()

