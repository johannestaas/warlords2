#
# This file is part of warlords.
# 
# warlords is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# warlords is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with warlords.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python
''' UI
implemented with pygame
'''

import pygame
from pygame.locals import *
from war import LOG

class UI(object):
    ''' UI for client
    '''
    OPEN = False
    WIDTH = 640
    HEIGHT = 480
    
    def __init__(self, game):
        ''' singleton UI instance
        Sets up the screen and background.
        '''
        # singleton
        if UI.OPEN:
            raise RuntimeError('Should only have one UI running.')
        else:
            UI.OPEN = True

        self.screen = pygame.display.set_mode((UI.WIDTH, UI.HEIGHT))
        self.back = pygame.Surface(self.screen.get_size())
        # grey
        self.back.fill((64, 96, 96))
        self.screen.blit(self.back, (0, 0))
        # game state
        self.game = game

    def loop(self):
        while UI.OPEN:
            pygame.display.flip()
            for event in pygame.event.get():
                if event.type != KEYDOWN:
                    continue
                if event.key == K_ESCAPE:
                    UI.OPEN = False
                    break

