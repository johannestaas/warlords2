#
# This file is part of warlords.
# 
# warlords is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# warlords is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with warlords.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python

import socket
import struct
import threading

import war.state
import war.ui
from war.net import connect, poll_server
from war.tk import ChatWindow
from war import LOG

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('host', help='ip address of the server')
    parser.add_argument('-p', '--port', default=9900, type=int, 
        help='server port')
    parser.add_argument('-l', '--log', default='war_server.log',
        help='path to log')
    parser.add_argument('-d', '--debug', default=False, action='store_true',
        help='set logging level to debug')
    parser.add_argument('-q', '--quiet', default=False, action='store_true',
        help='suppress output to console')
    args = parser.parse_args()

    # initiate connection
    war.net.connect(args.host, args.port)
    # start receiving
    net_t = threading.Thread(target=poll_server)
    net_t.name = 'net'
    net_t.start()
    # start chat window
    chat_t = threading.Thread(target=ChatWindow)
    chat_t.name = 'chat'
    chat_t.start()
    LOG.debug('Starting warlords pygame UI')
    game = war.state.Game()
    ui = war.ui.UI(game)
    ui.loop()
    war.net.disconnect()
    chat_t.join()
    net_t.join()
