warlords
========

phases of death
* Build
* Tactics
* Sorcery
* Combat
* Recruit

### Build

* Construct walls, towers, guilds
* Upgrade each
* Tetris-ish?

During this phase, you construct your buildings, which will either provide
defense for your units to hide in or protect your tech buildings, which open up
new units, buildings, formations and spells.

### Tactics

* ALL units walk forward a Xi spaces.
* Formations (Line, Vanguard, Square, Diamond...)
* Certain troops get large bonuses based on formation
* Some troops cannot use some formations
* Higher order formations might need a tech building

Pick a formation and "place" it on a squad of troops.
It forces them to face certain directions, making them stay that way.

### Sorcery

* Cast spells that buff or debuff, damage, etc...
* Dependent on buildings that give tech
* Hero starts out with a spell or two

Pretty self explanatory. Make them varied!

### Combat

* Formations attack
* Some units retaliate
* If a unit dies, it breaks the formation

The tactics might be to target certain units, disabling formations.
I don't want people to be overly defensive. Maybe it takes up too
much space to have units at your base?

### Recruit

* Spawn units near certain buildings
* Need tech for some
* Start with certain upgrades dependent on buildings?

Basically, highlight 3 buildings and they spawn units, or some resource
based idea.

