#
# This file is part of warlords.
# 
# warlords is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# warlords is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with warlords.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python
''' war_server
serves a warlords game
'''

import os
import war.server

import logging
from war import LOG

def start_server(host='localhost', port=9900):
    server = war.server.WarServer((host, port), war.server.WarHandler)
    server.start()
    LOG.info('server running on port %d' % port)
    return server

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', default=9900, type=int, 
        help='listening port')
    parser.add_argument('--local', default=False, action='store_true',
        help='only serve to localhost')
    parser.add_argument('-l', '--log', default='war_server.log',
        help='path to log')
    parser.add_argument('-d', '--debug', default=False, action='store_true',
        help='set logging level to debug')
    parser.add_argument('-q', '--quiet', default=False, action='store_true',
        help='suppress output to console')
    args = parser.parse_args()
    port = args.port
    host = 'localhost' if args.local else '0.0.0.0'

    LOG = logging.getLogger('war_server')
    LOG_FMT = logging.Formatter(
        '[%(asctime)s] %(name)s (%(levelname)s): %(message)s')
    if args.debug:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    LOG.setLevel(log_level)
    if not args.quiet:
        strm = logging.StreamHandler()
        strm.setFormatter(LOG_FMT)
        LOG.addHandler(strm)
    fileh = logging.FileHandler(filename=args.log)
    fileh.setFormatter(LOG_FMT)
    LOG.addHandler(fileh)

    server = start_server(host=host, port=port)
    while True:
        LOG.info('Entered loop.')
        LOG.info('q to quit. l for chat logs')
        inp = raw_input().lower()
        if inp == 'q':
            break
        elif inp == 'l':
            war.server.print_all_chatlogs()

    server.kill()

